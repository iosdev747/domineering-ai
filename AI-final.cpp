///////////////////////  MADE BY- Arsh and Anshul /////////////////////
///////////////////////       Team - iOSDev       /////////////////////
///////////////////// Code-Warriors 2019 Runner up ////////////////////


#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <random>
#include <map>

#define BOARD_ROW 13
#define BOARD_COL 13
#define BOMB_SIZE 5
using namespace std;

enum orientation {
    EMPTY = 0,
    HORIZONTAL = 1,
    VERTICAL = 2
};
enum move_type {
    NORMALMOVE = 1,
    BOMBMOVE = 2
};

orientation board[BOARD_ROW][BOARD_COL];
orientation chunk[BOMB_SIZE][BOMB_SIZE];
clock_t start;
bool breakedInMiddle = false;
orientation me;
bool haveBomb;

//uncomment all comments if want to use zobrist hashing :)

/*
unsigned long long int ZobristTable[BOARD_ROW][BOARD_COL];
map<unsigned long long int, int> TrasnportationTable;
mt19937 mt(01234567);

unsigned long long int randomInt() {
    uniform_int_distribution<unsigned long long int> dist(0, UINT64_MAX);
    return dist(mt);
}

void initTable() {
    for (int i = 0; i < BOARD_ROW; i++)
        for (int j = 0; j < BOARD_COL; j++)
            ZobristTable[i][j] = randomInt();
}

unsigned long long int computeHash() {
    unsigned long long int h = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (board[i][j] != EMPTY) {
                h ^= ZobristTable[i][j];
            }
        }
    }
    return h;
}
*/

bool compare(const pair<pair<int, int>, pair<int, int> > &p1, const pair<pair<int, int>, pair<int, int> > &p2) {
    return p1.first.first > p2.first.first;
}

bool compare2(const pair<pair<int, int>, pair<int, int> > &p1, const pair<pair<int, int>, pair<int, int> > &p2) {
    return p1.first.first < p2.first.first;
}

bool setBoard(orientation, int, int);

bool haveTime();

bool setBoard(orientation player, int x, int y) {
    if (player == HORIZONTAL) {
        if (x >= BOARD_ROW || y + 1 >= BOARD_COL || board[x][y] != EMPTY || board[x][y + 1] != EMPTY || x < 0 || y < 0)
            return false;
        board[x][y] = player;
        board[x][y + 1] = player;
        return true;
    } else {
        if (x + 1 >= BOARD_ROW || y >= BOARD_COL || board[x][y] != EMPTY || board[x + 1][y] != EMPTY || x < 0 || y < 0)
            return false;
        board[x][y] = player;
        board[x + 1][y] = player;
        return true;
    }
}

bool setBomb(int x, int y) {
    int HCount = 0, VCount = 0;
    if (x >= 0 && x <= BOARD_ROW - BOMB_SIZE && y >= 0 && y <= BOARD_COL - BOMB_SIZE) {           //HORIZONTAL PLAYER
        for (int i = x; i < x + BOMB_SIZE; i++) {
            HCount = 0;
            for (int j = y; j < y + BOMB_SIZE; j++) {
                if (board[i][j] == HORIZONTAL) {
                    HCount++;
                } else if (HCount % 2 == 1) {
                    return false;
                }
            }
            if (HCount % 2 == 1) {
                return false;
            }
        }
        for (int i = y; i < y + BOMB_SIZE; i++) {                                                 //VERTICAL PLAYER
            VCount = 0;
            for (int j = x; j < x + BOMB_SIZE; j++) {
                if (board[j][i] == VERTICAL) {
                    VCount++;
                } else if (VCount % 2 == 1) {
                    return false;
                }
            }
            if (VCount % 2 == 1) {
                return false;
            }
        }
        for (int i = x; i < x + BOMB_SIZE; ++i) {
            for (int j = y; j < y + BOMB_SIZE; ++j) {
                board[i][j] = EMPTY;
            }
        }
        return true;
    } else {
        return false;
    }
}

void resetBoard(orientation player, int x, int y) {
    if (player == HORIZONTAL) {
        board[x][y] = EMPTY;
        board[x][y + 1] = EMPTY;
    } else {
        board[x][y] = EMPTY;
        board[x + 1][y] = EMPTY;
    }
}

void cloneBombArea(int x, int y){
    for (int i = x, p=0; i < x + BOMB_SIZE; i++,p++) {
        for (int j = y, q=0; j < y + BOMB_SIZE; j++,q++) {
            chunk[p][q] = board[i][j];
        }
    }
}

void recoverBombArea(int x, int y) {
    for (int i = x, p=0; i < x + BOMB_SIZE; i++,p++) {
        for (int j = y, q=0; j < y + BOMB_SIZE; j++,q++) {
            board[i][j] = chunk[p][q];
        }
    }
}

int calcRealMoves(orientation player) {
    int moves = 0;
    if (player == HORIZONTAL) {
        for (int i = 0; i < BOARD_ROW; i++) {
            for (int j = 0; j < BOARD_COL - 1; j++) {
                if (board[i][j] == EMPTY && board[i][j + 1] == EMPTY) {
                    moves++;
                }
            }
        }
    } else {
        for (int i = 0; i < BOARD_ROW - 1; i++) {
            for (int j = 0; j < BOARD_COL; j++) {
                if (board[i][j] == EMPTY && board[i + 1][j] == EMPTY) {
                    moves++;
                }
            }
        }
    }
    return moves;
}

int calcSafeMoves(orientation player) {
    int moves=0;
    if(player==HORIZONTAL) {
        for(int j=0;j<BOARD_COL-1;j++){
            if(board[0][j]==EMPTY && board[0][j+1]==EMPTY && board[1][j]!=EMPTY && board[1][j+1]!=EMPTY )
                moves++;
            if(board[BOARD_ROW-1][j]==EMPTY && board[BOARD_ROW-1][j+1]==EMPTY && board[BOARD_ROW-2][j]!=EMPTY && board[BOARD_ROW-2][j+1]!=EMPTY )
                moves++;
        }
        for(int i=1;i<BOARD_ROW-1;i++){
            for(int j=0;j<BOARD_COL-1;j++){
                if(board[i][j]==EMPTY && board[i][j+1]==EMPTY && board[i-1][j]!=EMPTY && board[i-1][j+1]!=EMPTY && board[i+1][j]!=EMPTY && board[i+1][j+1]!=EMPTY)
                    moves++;
            }
        }
    }
    else {
        for(int i=0;i<BOARD_ROW-1;i++){
            if(board[i][0]==EMPTY && board[i+1][0]==EMPTY && board[i][1]!=EMPTY && board[i+1][1]!=EMPTY )
                moves++;
            if(board[i][BOARD_COL-1]==EMPTY && board[i+1][BOARD_COL-1]==EMPTY && board[i][BOARD_COL-2]!=EMPTY && board[i+1][BOARD_COL-2]!=EMPTY )
                moves++;
        }
        for(int i=0;i<BOARD_ROW-1;i++){
            for(int j=1;j<BOARD_COL-1;j++){
                if(board[i][j]==EMPTY && board[i+1][j]==EMPTY && board[i][j-1]!=EMPTY && board[i][j-1]!=EMPTY && board[i+1][j+1]!=EMPTY && board[i+1][j+1]!=EMPTY)
                    moves++;
            }
        }
    }
    return moves;
}

int evaluate(orientation player) {
    int realMoves_Player = calcRealMoves(player);
    int realMoves_Opponent = ((player == HORIZONTAL) ? calcRealMoves(VERTICAL) : calcRealMoves(HORIZONTAL));
    int safeMoves_Player = calcSafeMoves(player);
    int safeMoves_Opponent =  ((player == HORIZONTAL) ? calcSafeMoves(VERTICAL) : calcSafeMoves(HORIZONTAL));
    return realMoves_Player + 2*safeMoves_Player - realMoves_Opponent - 2*safeMoves_Opponent;
}

int alphabeta(int depth, orientation player, int &ri, int &rj, int alpha, int beta, move_type &moveType) {
    if (depth == 0 || !haveTime())
        return evaluate(player);

    orientation opponent = (player==HORIZONTAL)?VERTICAL:HORIZONTAL;

    vector<pair<pair<int, int>, pair<int, int> > > moves;
    pair<int, int> coords,val;
    pair<pair<int, int>, pair<int, int> > singleMove;
    for (int i = 0; i < BOARD_ROW; i++) {
        for (int j = 0; j < BOARD_COL; j++) {
            if (setBoard(player, i, j)) {
                int value = evaluate(player);
                coords = make_pair(i, j);
                val = make_pair(value,NORMALMOVE);
                singleMove = make_pair(val, coords);
                moves.push_back(singleMove);
                resetBoard(player, i, j);
            }
        }
    }

    if((calcSafeMoves(opponent)>calcRealMoves(player)) && haveBomb){
        for (int i = 0; i <= BOARD_ROW-BOMB_SIZE; i++) {
            for (int j = 0; j <= BOARD_COL-BOMB_SIZE; j++) {
                cloneBombArea(i,j);
                if (setBomb(i, j)) {
                    int value = evaluate(player);
                    coords = make_pair(i, j);
                    val = make_pair(value,BOMBMOVE);
                    singleMove = make_pair(val, coords);
                    moves.push_back(singleMove);
                    recoverBombArea(i, j);
                }
            }
        }
    }
    //else{//lose}
    if (me == player) sort(moves.begin(), moves.end(), &compare);
    else sort(moves.begin(), moves.end(), &compare2);

//    map<unsigned long long int, int>::iterator it;
    int fi = 0;
    int fj = 0;
    move_type mv = NORMALMOVE;
    for (int k = 0; k < moves.size(); k++) {
        int i = moves[k].second.first;
        int j = moves[k].second.second;
        if(moves[k].first.second==NORMALMOVE){
            if (setBoard(player, i, j)) {
                /*
//                int value;
//                it = TrasnportationTable.find(computeHash());
//                if(it == TrasnportationTable.end()) {
//                    value = -alphabeta(depth - 1, ((player == HORIZONTAL) ? VERTICAL : HORIZONTAL), fi, fj, -beta, -alpha, moveType);
//                    TrasnportationTable.insert(pair<unsigned long long int, int>(computeHash(),evaluate(player)));
//                }else{
//                    value = it->second;
//                }
*/
//  uncomment above section and comment below line for zorbist hashing
                int value = -alphabeta(depth - 1, ((player == HORIZONTAL) ? VERTICAL : HORIZONTAL), fi, fj, -beta, -alpha, mv);
                resetBoard(player, i, j);
                if (value > alpha) {
                    moveType=NORMALMOVE;
                    alpha = value;
                    ri = i;
                    rj = j;
                    if (alpha >= beta) {
                        return beta;
                    }
                }
            }
        }
        else{
            cloneBombArea(i, j);
            if (setBomb(i, j)) {
                haveBomb = false;
                /*
//                int value;
//                it = TrasnportationTable.find(computeHash());
//                if(it == TrasnportationTable.end()) {
//                    value = -alphabeta(depth - 1, ((player == HORIZONTAL) ? VERTICAL : HORIZONTAL), fi, fj, -beta, -alpha, moveType);
//                    TrasnportationTable.insert(pair<unsigned long long int, int>(computeHash(),evaluate(player)));
//                }else{
//                    value = it->second;
//                }
*/
//  uncomment above section and comment below line for zorbist hashing
                int value = -alphabeta(depth - 1, ((player == HORIZONTAL) ? VERTICAL : HORIZONTAL), fi, fj, -beta, -alpha, mv);
                haveBomb = true;
                recoverBombArea(i, j);
                if (value > alpha) {
                    moveType=BOMBMOVE;
                    alpha = value;
                    ri = i;
                    rj = j;
                    if (alpha >= beta) {
                        return beta;
                    }
                }
            }
        }
        if (!haveTime()) {
            breakedInMiddle = true;
            return alpha;
        }
    }
    return alpha;
}

bool haveTime() {
    clock_t finish = clock();
    return (double) (finish - start) / CLOCKS_PER_SEC <= 0.85;
}

void iterativeDeepning(int &resultX, int &resultY, move_type &moveType) {
    start = clock();
    int depth = 2;
    int resultx = resultX, resulty = resultY;
    move_type mv = moveType;
    while (haveTime()) {
        alphabeta(depth, me, resultx, resulty, -10000, 10000, mv);
        if (!breakedInMiddle) {
            resultX = resultx;
            resultY = resulty;
            moveType = mv;
        }
        depth++;
    }
}

int main() {
    int temp;
    std::cin >> temp;
    me = (temp == 1) ? HORIZONTAL : VERTICAL;
    for (int i = 0; i < BOARD_ROW; i++) {
        for (int j = 0; j < BOARD_COL; j++) {
            std::cin >> temp;
            board[i][j] = (temp == 0) ? EMPTY : (temp == 1 ? HORIZONTAL : VERTICAL);
        }
    }
    std::cin >> temp;
    haveBomb = temp == 1;
    int i = 0, j = 0;
    move_type moveType = NORMALMOVE;
    iterativeDeepning(i, j, moveType);
    if (moveType == NORMALMOVE) { temp = 1; } else { temp = 2; }
    std::cout << temp << " " << i << " " << j;
    return 0;
}